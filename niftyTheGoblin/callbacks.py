import os
import torch

from .train import Agent, state_to_features, ACTIONS

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')




def setup(self):
    continueTraining = False
    model_path = 'tmp/'

    if (not continueTraining and self.train): # or not os.path.isfile(model_path)
        self.logger.info("Setting up model from scratch.")
        print("Setting up model from scratch.")
        self.agent = Agent(gamma=0.99, epsilon=1., lr=1e-7,
                              input_dims=(17, 17, 1), eps_dec=1e-6,
                              n_actions=6, max_mem_size=100000, batch_size=128,
                              eps_end=0.01, replace=2000, fname=model_path, train=self.train, use_cpu=False)
    else:
        self.logger.info("Loading model from saved state.")
        print("Loading model from saved state.")
        self.agent = Agent(gamma=0.99, epsilon=1., lr=1e-7,
                           input_dims=(17, 17, 1), eps_dec=1e-6,
                           n_actions=6, max_mem_size=100000, batch_size=128,
                           eps_end=0.01, replace=2000, fname=model_path, train=self.train, use_cpu=True)
        self.agent.load_model()


def act(self, game_state: dict) -> str:
    observation = state_to_features(game_state)
    actions = ACTIONS[self.agent.choose_action(observation)]
    return actions
